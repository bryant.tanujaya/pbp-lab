from functools import total_ordering
from os import makedirs
from django.contrib.admin.options import ModelAdmin
from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=40)
    From = models.CharField(max_length=40)
    title = models.CharField(max_length=50)
    message = models.CharField(max_length=10000)


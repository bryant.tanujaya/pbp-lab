from django.urls import path
from .views import check, index

urlpatterns = [
    path('', index, name='index'), 
    path('friends', check, name='friend')
    # TODO Add friends path using friend_list Views
]
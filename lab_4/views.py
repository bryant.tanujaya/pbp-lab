from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    note = Note.objects.all()
    response = {'Notes': note}
    return render(request, 'lab4_index.html', response)

@login_required(login_url="/admin/login/")
def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    else:
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})
    
@login_required(login_url="/admin/login/")
def note_list(request):
    note = Note.objects.all()
    response = {'Notes': note}
    return render(request, 'lab4_note_list.html', response)
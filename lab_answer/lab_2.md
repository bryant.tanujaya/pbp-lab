1. What are the difference between JSON and XML?

There are many difference between JSON and XML. First of all JSON stands for JavaScript Object Notation (The extension file of JSON file is .json), and XML stands for eXtensible Markup Language (The extension file of XML file is .xml). In determining the speed to read and write JSON is quicker than the XML because the learning curve is higher. For the next case in the case of the file size JSON file size is smaller if compared to the XML file size. And for the last difference is in case of security JSON file is less secure than the XML file. The JSON is based on data-oriented. In contrast, the XML file is document-oriented.

Source : (https://www.javatpoint.com/json-vs-xml)

2. What are the difference between HTML and XML?

For the introduction, HTML stands for HyperText Markup Language. The main focus for HTML is used to display data and how data looks. On the other side, XML is focused to transport and store data. HTML is not case sensitive language, but XML does. HTML itself is a markup language itself, unlike XML which provides a framework to define markup languages. The last difference that the programmer often see is that HTML is not necessary to use a closing tag and does not preserve whitespaces, but for XML it is a must to use a closing tag and preserve whitespaces.